import json
import pathlib
import unidecode

import spotipy
from spotipy.oauth2 import SpotifyOAuth


SPOTIPY_CLIENT_ID='c581cc3278234c1db4ad5196cee3b0a6'
SPOTIPY_CLIENT_SECRET='eeb127b88f224c55b72b622473acba8e'
SPOTIPY_REDIRECT_URI='http://localhost:5000'

sp = spotipy.Spotify(auth_manager=SpotifyOAuth(
    client_id=SPOTIPY_CLIENT_ID,
    client_secret=SPOTIPY_CLIENT_SECRET,
    redirect_uri=SPOTIPY_REDIRECT_URI,
    scope="playlist-read-private"
))

def get_playlist_user(sp):
    playlists = sp.current_user_playlists()

    all_playlist = []

    while playlists:
        for playlist in playlists['items']:
            print(playlist['name'])
            playlist_name_not_bad_chars = ''.join(
                map(lambda x: x if x not in [';', ':', '!', "*", "(", ")"] else '', playlist['name'])
            )
            playlist_name_unicode = unidecode.unidecode(playlist_name_not_bad_chars)
            playlist_dict = {'id': playlist['id'], 'name': playlist_name_unicode }
            all_playlist.append(playlist_dict)    

        if playlists['next']:
            playlists = sp.next(playlists)
        else:
            playlists = None
    
    return all_playlist


def get_tracks_from_all_playlist_pages(playlist):
    all_tracks = []
    while playlist:
        for track in playlist['items']:
            all_tracks.append({
                'name': track['track']['name'],
                'artists':[artist['name'] for artist in track['track']['artists']]
            })
        if playlist['next']:
            playlist = sp.next(playlist)
        else:
            playlist = None
    
    return {"tracks": all_tracks, "count": len(all_tracks)}


def create_json_file(playlist_name, playlist_tracks):
    pathlib.Path('playlists').mkdir(parents=True, exist_ok=True) 
    with open(f'playlists/{playlist_name}.json','w') as f:
        f.write(json.dumps(playlist_tracks))

if __name__ == '__main__':

    # all playlists
    user_playlists = get_playlist_user(sp)
    for playlist in user_playlists:
        playlist_tracks = sp.playlist_tracks(playlist['id'])
        dict_playlist_tracks = get_tracks_from_all_playlist_pages(playlist_tracks)
        create_json_file(playlist['name'], dict_playlist_tracks)

    # saved songs
    saved_tracks = sp.current_user_saved_tracks()
    user_saved_songs = get_tracks_from_all_playlist_pages(saved_tracks)
    create_json_file('Curtidas', user_saved_songs)
